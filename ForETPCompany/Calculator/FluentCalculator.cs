namespace ForETPCompany.Calculator;

public class FluentCalculator
{
	public CalculatorValue Zero => new(0);
	public CalculatorValue One => new(1);
	public CalculatorValue Two => new(2);
	public CalculatorValue Three => new(3);
	public CalculatorValue Four => new(4);
	public CalculatorValue Five => new(5);
	public CalculatorValue Six => new(6);
	public CalculatorValue Seven => new(7);
	public CalculatorValue Eight => new(8);
	public CalculatorValue Nine => new(9);
	public CalculatorValue Ten => new(10);
}