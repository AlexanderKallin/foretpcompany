namespace ForETPCompany.Calculator;

public class CalculatorValue : ICalculatorExpressionElement
{
	private static readonly Dictionary<CalculatorOperationType, Func<double, double, double>>
		CalculatorOperationDelegateMap =
			new()
			{
				[CalculatorOperationType.Plus] = (a, b) => a + b,
				[CalculatorOperationType.Minus] = (a, b) => a - b,
				[CalculatorOperationType.Times] = (a, b) => a * b,
				[CalculatorOperationType.DividedBy] = (a, b) => a / b
			};

	public readonly double Value;

	public CalculatorValue(double value)
	{
		Value = value;
	}

	public CalculatorValue(ICalculatorExpressionElement parent, double value)
	{
		Parent = parent;
		Value = value;
	}

	public CalculatorOperation Plus => new(this, CalculatorOperationType.Plus);
	public CalculatorOperation Minus => new(this, CalculatorOperationType.Minus);
	public CalculatorOperation Times => new(this, CalculatorOperationType.Times);
	public CalculatorOperation DividedBy => new(this, CalculatorOperationType.DividedBy);

	public ICalculatorExpressionElement? Parent { get; init; }

	private double Result()
	{
		if (Parent == null)
			return Value;

		var expression = CollectExpression();

		try
		{
			PerformTimesAndDividedByOperations(expression);
		}
		catch (DivideByZeroException)
		{
			return double.PositiveInfinity;
		}

		return PerformPlusAndMinusOperationsAndCalculateResult(expression);
	}

	private List<ICalculatorExpressionElement> CollectExpression()
	{
		var expression = new List<ICalculatorExpressionElement>
		{
			this
		};

		var expressionElement = Parent;
		while (expressionElement != null)
		{
			expression.Add(expressionElement);
			expressionElement = expressionElement.Parent;
		}

		expression.Reverse();
		return expression;
	}

	private static void PerformTimesAndDividedByOperations(List<ICalculatorExpressionElement> expression)
	{
		for (var i = expression.Count - 1; i >= 0; i--)
		{
			if (expression[i] is CalculatorOperation
			    {
				    Type: CalculatorOperationType.Times or CalculatorOperationType.DividedBy
			    } operation)
			{
				var leftNumber = ((CalculatorValue)expression[i - 1]).Value;
				var rightNumber = ((CalculatorValue)expression[i + 1]).Value;

				if (operation.Type == CalculatorOperationType.DividedBy && rightNumber == 0)
					throw new DivideByZeroException();

				var operationDelegate = CalculatorOperationDelegateMap[operation.Type];

				var newValue = operationDelegate.Invoke(rightNumber, leftNumber);

				expression.RemoveRange(i - 1, 3);
				expression.Insert(i - 1, new CalculatorValue(newValue));
			}
		}
	}

	private static double PerformPlusAndMinusOperationsAndCalculateResult(List<ICalculatorExpressionElement> expression)
	{
		var result = ((CalculatorValue)expression[0]).Value;
		for (var i = 1; i < expression.Count; i += 2)
		{
			var operation = (CalculatorOperation)expression[i];
			var rightNumber = ((CalculatorValue)expression[i + 1]).Value;
			var operationDelegate = CalculatorOperationDelegateMap[operation.Type];

			result = operationDelegate.Invoke(result, rightNumber);
		}

		return result;
	}

	public static implicit operator double(CalculatorValue thisValue)
	{
		return thisValue.Result();
	}
}