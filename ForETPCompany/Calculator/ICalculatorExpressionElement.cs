namespace ForETPCompany.Calculator;

public interface ICalculatorExpressionElement
{
	public ICalculatorExpressionElement? Parent { get; }
}