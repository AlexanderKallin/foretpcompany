namespace ForETPCompany.Calculator;

public class CalculatorOperation : ICalculatorExpressionElement
{
	public readonly CalculatorOperationType Type;

	public CalculatorOperation(ICalculatorExpressionElement parent, CalculatorOperationType type)
	{
		Parent = parent;
		Type = type;
	}

	public CalculatorValue Zero => new(this, 0);
	public CalculatorValue One => new(this, 1);
	public CalculatorValue Two => new(this, 2);
	public CalculatorValue Three => new(this, 3);
	public CalculatorValue Four => new(this, 4);
	public CalculatorValue Five => new(this, 5);
	public CalculatorValue Six => new(this, 6);
	public CalculatorValue Seven => new(this, 7);
	public CalculatorValue Eight => new(this, 8);
	public CalculatorValue Nine => new(this, 9);
	public CalculatorValue Ten => new(this, 10);

	public ICalculatorExpressionElement? Parent { get; init; }
}