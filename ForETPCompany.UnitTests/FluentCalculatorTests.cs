using FluentAssertions;
using ForETPCompany.Calculator;

namespace ForETPCompany.UnitTests;

public class FluentCalculatorTests
{
	[SetUp]
	public void Setup()
	{
		_calculator = new FluentCalculator();
	}

	private FluentCalculator _calculator = null!;
	
	[Test]
	public void OneShouldReturnOne()
	{
		double result = _calculator.One;

		result.Should().Be(1);
	}

	[Test]
	public void TwoPlusTwoShouldBeFour()
	{
		double result = _calculator.Two.Plus.Two;

		result.Should().Be(4);
	}
	
	[Test]
	public void TwoMinusTwoShouldBeZero()
	{
		double result = _calculator.Two.Minus.Two;

		result.Should().Be(0);
	}
	
	[Test]
	public void TwoTimesTwoShouldBeFour()
	{
		double result = _calculator.Two.Times.Two;

		result.Should().Be(4);
	}
	
	[Test]
	public void TwoDividedByTwoShouldBeOne()
	{
		double result = _calculator.Two.DividedBy.Two;

		result.Should().Be(1);
	}
	
	[Test]
	public void TwoDividedByZeroShouldBeInfinity()
	{
		double result = _calculator.Two.DividedBy.Zero;

		result.Should().Be(double.PositiveInfinity);
	}
	
	[Test]
	public void TwoDividedByZeroPlusTwoShouldBeInfinity()
	{
		double result = _calculator.Two.DividedBy.Zero.Plus.Two;

		result.Should().Be(double.PositiveInfinity);
	}
	
	[Test]
	public void TwoPlusTwoTimesTwoShouldBeSix()
	{
		double result = _calculator.Two.Plus.Two.Times.Two;

		result.Should().Be(6);
	}
	
	[Test]
	public void TwoPlusTwoTimesTwoShouldBeFour()
	{
		double result = _calculator.Two.Times.Two.DividedBy.Two.Times.Two;

		result.Should().Be(4);
	}
	
	[Test]
	public void TenMinusFivePlusFiveDividedByFiveShouldBeSix()
	{
		double result = _calculator.Ten.Minus.Five.Plus.Five.DividedBy.Five;

		result.Should().Be(6);
	}
	
	
	[Test]
	public void OnePlusTwoPlusThreeMinusOneMinusTwoMinusFourShouldBeMinusOne()
	{
		var result = _calculator.One.Plus.Ten - 10;

		result.Should().Be(1);
	}
	
	[Test]
	public void ShouldResolveToPrimitiveInteger()
	{
		double result = _calculator.One.Plus.Two.Plus.Three.Minus.One.Minus.Two.Minus.Four;

		result.Should().Be(-1);
	}
}