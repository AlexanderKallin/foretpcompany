using FluentAssertions;

namespace ForETPCompany.UnitTests;

public class SomeBoolTests
{
	[Test]
	public void ShouldBeEqualTrueAndFalse()
	{
		var someBool = new SomeBool();

		var result = someBool == true && someBool == false;

		result.Should().Be(true);
	}
}